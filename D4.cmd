Executable = /bin/bash
Universe   = vanilla
getenv     = true
input      = 
output	   = D4eval.out
error      = D4eval.error
Log        = D4eval.log
arguments  = D4eval.sh
transfer_executable = false
request_memory = 4*1024
Queue