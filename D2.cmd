Executable = /bin/bash
Universe   = vanilla
getenv     = true
input      = 
output     = D2.out
error      = D2.error
Log        = D2.log
arguments  = D2.sh
transfer_executable = false
request_memory = 4*1024
Queue