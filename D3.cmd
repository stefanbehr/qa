Executable = /bin/bash
Universe   = vanilla
getenv     = true
input      = 
output     = D3.out
error      = D3.error
Log        = D3.log
arguments  = D3.sh
transfer_executable = false
request_memory = 4*1024
Queue