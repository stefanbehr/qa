QA_system.py by  Behr, Bodding-Long, Walters
    Evaluates answers based on Questions from a given XML fine.
    Answers are supported by a document in the LDC ACQUAINT corpus


This system attempts to include the answer and only the answer, all answers given are under 100 characters
    Maximum Lengths:
        2007: 51c
        2006: 69c
    
Automatic running of the full program can be ran using:
sh D*.sh
or
condor_submit D*.cmd
Replacing '*' with the deliverable number
    
The entrance to the program is qa/src/QA_System.py
    Allow ~20m for Lucene to build an index of ACQUAINT on patas.

For any help on running QA_system.py call:
    python2.7 QA_system.py --help
    
Commands:
To run without a .sh script:
Replace '[No. Deliverable]' with the proper label

        python2.7 src/QA_system.py -a /dropbox/12-13/573/Data/patterns/devtest/factoid-docs.litkowski.2006.txt -q /dropbox/12-13/573/Data/Questions/devtest/TREC-2006.xml -r D2Run -o outputs/[No. Deliverable]

To individually re-run scores:

    python2.6 /opt/dropbox/12-13/573/code/compute_mrr.py /dropbox/12-13/573/Data/patterns/devtest/factoid-docs.litkowski.2006.txt outputs/[No. Deliverable].outputs lenient > results/[No. Deliverable].results_lenient
    
    python2.6 /opt/dropbox/12-13/573/code/compute_mrr.py /dropbox/12-13/573/Data/patterns/devtest/factoid-docs.litkowski.2006.txt outputs/[No. Deliverable].outputs strict > results/[No. Deliverable].results_strict