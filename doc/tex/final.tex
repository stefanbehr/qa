%
% File acl-hlt2011.tex
%
% Contact: gdzhou@suda.edu.cn
%%
%% Based on the style files for ACL2008 by Joakim Nivre and Noah Smith
%% and that of ACL2010 by Jing-Shin Chang and Philipp Koehn


\documentclass[10pt]{article}
\usepackage[margin=1in]{geometry}

% Nick's packages
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage[justification=justified,singlelinecheck=false]{caption}
\usepackage{xtab}
\usepackage{longtable}
\usepackage{epstopdf}
\usepackage{booktabs}
% for the NYC theme 
\usepackage{array}
\usepackage{colortbl}
\usepackage{xcolor}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{acl-hlt2011}
\usepackage{times}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{url,ctable}
\DeclareMathOperator*{\argmax}{arg\,max}
\setlength\titlebox{6.5cm}    % Expanding the titlebox

\title{Open Domain Factoid Question Answering: An Iterative Approach}

\author{Stefan Behr \\
  University of Washington\\
  {\tt sbehr} \\\And
  Tristan Bodding-Long \\
  University of Washington\\
  {\tt blta} \\
  @ u dot washington dot edu\And
  Nicholas Waltner \\
  University of Washington\\
  {\tt nwaltner} \\
  }

\date{}

\begin{document}
\maketitle
\pagenumbering{roman} 
\begin{abstract}
 Over the course of this semester, we have taken an iterative approach to building an end-to-end Question Answering (``QA") system utilizing tools from Information Retrieval (``IR") and Natural Language Processing (``NLP").  Our final and best model produced a lenient MRR score of 0.2559 and a strict score of 0.125 with no unanswered questions, a first guess accuracy of 18.6\%, and a run-time of 4 minutes on our development data. On our test data, the TREC 2007 QA question set, we scored a lenient MRR of 0.2313, strict score of 0.89, and an accuracy of 15.86\%.  Throughout our system design and implementation process, we were careful to note intermediate results as we attempted various enhancements to the system.  Additionally, we spent significant amounts of time on error and parameter checking, which proved to provide significant improvements.  The areas which significantly improved our results were question classification and reformulation, anaphora resolution, answer type boosting, and answer diversification. 
  \end{abstract}

\section{Introduction}
The initial thrust of our IR/QA engine was to emulate the work of the ARANEA project utilizing a redundant shallow approach.  In each successive deliverable we simply explored a number of techniques discussed that focused on specific aspects of the system and evaluated which technique(s) improved our overall performance. (\cite{ferrucci2010building} and \cite{hirschman2001natural}). Naturally, if our performance improved with an implemented technique we kept it in the system; if it did not, then we discarded it.

\section{System Overview}
Our system follows the basic seven-step approach shown in Figure~\ref{fig:diagram} below.
\begin{figure*} 
\centering
\caption{System Architecture}

\includegraphics[scale=0.4]{diagram.png}
%\includegraphics[width=\textwidth]{diagram.png}
\label{fig:diagram}

\end{figure*}

 

\begin{table*}[ht!]
%\scriptsize
\footnotesize

\centering
\caption{\textbf{\small \sl  Stage by Stage Results}}

\begin{tabular}{lrrrrrl}
   %  &  \multicolumn{2}{c}{\textbf{Number of Expansions}}  \\
%\cmidrule(r){2-10}
Stage & Lenient&        Strict& Unanswered&     Accuracy&       Time(min)&      Note    \\
\toprule
D2&     0.0471& 0.0141& 0.2526& 0.0362& 45&     EV-QA Run, report baseline      \\
D2.1&   0.0683& 0.0227& 0.2526& 0.0543& 24&      Baseline: downweights words from the original query    \\
D2.2&   0.0848& 0.0319& 0.2526& 0.0620& 6&      nix 0-val from the back \\
D2.11&  0.1087& 0.0352& 0.2526& 0.0775& 8&      Using .7**, fixed snippet segmenting    \\
D3.4&   0.1234& 0.0436& 0.2270& 0.1189& 12&     Using 1, Pronoun/LSN resolution \\
D3.5&   0.1426& 0.0498& 0.2270& 0.1189& 12&     Using 1, Adding QA typing       \\
D3.6&   0.1816& 0.0555& 0.0561& 0.1499& 15&     Using 1, fixed up .pl for `In...'       \\
D3.10&  0.2106& 0.0569& 0.0561& 0.1499& 12&     Using .75**     \\
D3.11&  0.2223& 0.0604& 0.0568& 0.1628& 15&     .75**, put .lower() on my word in stopword checks. OOPS \\
D3.12&  0.2341& 0.0614& 0.0129& 0.1680& 9.1&    Python port installed   \\
D3.28&  0.2364& 0.0617& 0.0129& 0.1705& 6.3&    Treat focus words the same as query     \\
D4.1&   0.2559& 0.125& 0.0000& 0.1860& 4.2&    RM all 0-val bookends, Limit Web-results        \\
 \bottomrule
\end{tabular}
\label{tbl:shorttable}
\end{table*}
\newpage
\section{Approach}
We describe the various components of our system below:

\subsection{Redundancy-Based QA}
An important aspect of systems that have found success in previous iterations of the TREC competitions employs the intuition that a correct answer will occur often in large data sets such as the World Wide Web. This approach utilizes IR engines to return a set of documents most relevant to the input. The specific IR engine and the size of the data set are both important; larger datasets and `better' IR engines will return the correct answer more often, and reduce the need for input pre-processing.  A good IR engine will return a set of documents that are relevant to the input. Queries that are successfully transformed into input should result in documents that contain numerous references to the correct answer. In the following sections we explore our process of morphing a query into one or more search inputs, how our interface with the Internet works, the steps that we take to generate and rank candidate answer and finally the projection of answers onto a source document.

\subsection{Query Processing}
Prior to using the web to find answers for our queries, we were required to prepare our queries as search strings. The original query was given to us as a question string, and we were required to deal with anaphora and change the orthographic and syntactic properties of the query before using it as search input. In general, the main question word and any associated neighboring words (e.g., prepositions, verbs, etc.) were stripped out or relocated as needed, and the query was expanded appropriately given the context. For date and location queries, a number of expansions were used to increase redundancy.  In the event that none of the expansion or reformulation heuristics encoded in our query expansion module could be applied to a given query, the query was passed unchanged to our system's web search module, as a fallback measure.  Finally, we realized later that, given a sub-grouping of several questions, it is critical to use the sentence's topic in a query, or to use more sophisticated methods to replace `poor' query words such as pronouns.

\subsubsection{Anaphora Resolution}
In general, there were two types of in-query anaphora: pronouns and less-specific entities. It should be evident why searching for pronouns will not help a QA system, as the use of less-specific entities manifests itself in the shortening of entities such as ``82nd Airborne Division" to just ``division".  Both the pronouns and entity could refer to the answer of a prior question, or to the topic of a set of questions, or even to a prior entity in the query itself.  Sequenced references to another query's answer were not targeted, but would also be affected by the heavy-handed resolution strategy. In order to replace less specific entities with the most specific entity, the topic of each question set, generally a noun phrase, needed some preprocessing.  Some verb phrases and possessive clauses were also seen as question set topics.  A simple split of the topic on ``verby" words allowed us to pull out NP chunks, such as ``Thabo Mbeki" from ``Thabo Mbeki elected president".  Now that we had pulled out a specific entity, we needed to find the entity to which it referred.  Pronouns were simple to find and replace (we made no attempt to make gender or number agree). In order to replace within specific entities, we searched the query for overlapping words. If there was only one, but not every word of a three-word entity was included in a query, the resulting query would include the entire three-word entity. Table ~\ref{tbl:reformed} shows some examples of replacement, both successful and awkwardly phrased (but not necessarily useless).

\begin{table*} 
\scriptsize
\centering
\caption{\textbf{\small \sl  Query Entity Resolution}}

    \begin{tabular}{lll}
    Topic                           & Original                        & Revamped                                            \\
\toprule
    Warren Moon                     & Where did Moon play in college? & Where did Warren Moon play in college?              \\
    Britain's Prince Edward marries & Where did they honeymoon?       & Where did Britain's Prince Edward honeymoon?        \\
    American Enterprise Institute   & Where is the Institute located? & Where is the American Enterprise Institute located? \\
Leaning Tower of Pisa   & The Tower of Pisa began to be built in what year              & The Tower of Leaning Tower of Pisa began to be built in what year? \\
\bottomrule
    \end{tabular}
        
\label{tbl:reformed}
\end{table*}

\subsection{Question Classification}
Below we describe the approach to query classification, which we use to predict the type of answer for which we are looking. Using this prediction, we then look for answers that are reflective of that answer type.

\subsubsection{Methodology}
The methodology employed was that described in \cite{solorio2004language}, which employs a simple seven-category classification system consisting of:  Date, Measure, Place, Person, Object, Organization and Other.  We did see value in breaking these classifications into finer ones.  Additionally, given that we only had 592 training questions, it seemed that going to a 20- or 30-class hierarchy, as many systems do, we would likely suffer from data sparsity issues.

\subsubsection{Training}
The TREC 2003 and 2004 questions were imported into Excel and manually annotated into the seven aforementioned categories. There was some confusion whether a question whose answer was a country was a Place or an Organization and in many cases we opted for Organization.  Once we had fully populated the class labels, we ran each question though NLTK in Python to add POS tags. The questions were then output into their respective subdirectories and Mallet was used to build a Maximum Entropy bigram model. The training dataset confusion matrix results were extremely encouraging with only two misclassifications and an accuracy of 99.6\%.

\subsubsection{Test}
The MaxEnt model was then applied to the TREC 2006 \textit{devtest} dataset. The initial results seemed quite encouraging and we later hand-annotated this dataset as well, finding that the test accuracy was 82\%. Given such a simple model, this seemed to be quite a solid result. As one can see in Table \ref{tbl:confusionmatrix} below, Date, Measure and Person achieved accuracy rates of 90+\%. Organization and Other both had much higher rates of misclassification and Place had some issues, as often Date was included within a Place query. In our system, the answer typing was run once for all questions and saved until the answer reranking stage, at which point the typing information was used.
%\large
\thispagestyle{empty}

\begin{table}[ht!]
\footnotesize
\centering
\caption{\textbf{\small \sl  Confusion Matrix for TREC-2006 Questions\footnotemark[1]}}
\begin{tabular}{lrrrrrr}
Category & Date&Meas.&Org.&Other&Person&Place\\
\toprule

Date&68&0&0&7&0&4\\
Meas.&1&89&1&15&0&1\\
Object&0&0&1&0&0&0\\
Org.&1&1&6&8&1&3\\
Other&1&1&6&62&0&5\\
Person&0&0&0&7&54&0\\
Place&3&0&0&5&0&52\\
\midrule 
 \bottomrule
 

\end{tabular}
\label{tbl:confusionmatrix}

\end{table}
 \footnotetext[1]{Organization is abbreviated as Org. and Measure as Meas. for spacing purposes.} 

%\newpage


\subsection{Web Retrieval}

In order to search the web for document snippets pertaining to questions given to our system, we made use of Microsoft's Bing search engine. We interfaced with the search engine via the Bing Search API, using an open-source Python library\footnote[2]{https://github.com/xthepoet/pyBingSearchAPI.}.\\

Toward the beginning of each question-answering session, when our system's web search module is initialized, the module checks for a file on disk containing a cache of previous sessions. If no such file exists, an empty cache object is initialized for future storage of queries and their resulting lists of document snippets.\\

Upon receiving an expanded query from the query expansion module, our search module begins by checking for locally cached search results corresponding to the query. If the module's cache contains web document snippets associated with the given query, those snippets are returned immediately and no communication is required between our system and the Bing Search API.\\

In the event of a cache miss, the expanded query given to the web search module is sent to the Bing API (via the Python interface). The API response, which is received in the form of JSON data, contains a number of search results for the given query (our system retrieves the top fifty results for each query by default, which is the maximum number available per Bing API request). Each search result contains, among other information, a relevant document snippet.  All of the document snippets present in the API response data are extracted and then stored in the search module's cache along with the query string which generated the API response.  After saving document snippets to the module's cache, those snippets are then returned and passed along to the system's shallow ranker.\\

At the end of each question-answering session, the web search module's cache is serialized to a file for access in future sessions. This, combined with the fact that the search module reads in the same cache file at the beginning of each session, means that the cache always contains the search results from all questions asked of our system across all sessions (at least, until a different cache file is specified).\\

The use of such a persistent cache yields two major advantages. The first advantage is that of speed---replacing calls to a web-based API with local dictionary lookups for numerous queries drastically reduces running time. The second advantage is one of cost---avoiding numerous API calls per distinct query string helps us to stay within the request limits of the Bing Search API.\\

Our use of caching is not without downside. The current file size of our system's cache on disk is approximately 15 MB. This is reasonably small, yet the size of the cache could balloon as numerous novel sets of questions are introduced to our system. Furthermore, repetitive caching causes our system to potentially miss out on the advantages provided by the dynamic nature of the web, since a query's search results become static once cached. However, we do not expect that the search results for the majority of questions given to our system are likely to change much over time.

\subsubsection{Web Results}
To examine the efficacy of Bing's search engine and our queries we explored the accuracy of our system, as if we simply used the concatenated snippets from Bing as our answer to each query and recorded the accuracy in Figure~\ref{fig:acc}. This demonstrates a cap on our accuracy at 65.37\%, if we were to have a perfect answer extraction and processing algorithms.  The flattening of the curve's slope also demonstrates that the lower ranked web results from the IR engine are often not helpful. The last chunk of results could easily confuse our answer ranking algorithm, hence we explore shedding these results to raise our accuracy and MRR after describing the answer selection process.

\begin{figure*} 
\centering
\caption{\textbf{\small \sl  Web Result Accuracy}}

%\caption{Web Result Accuracy}
\includegraphics [width=\textwidth]{accuracy.png}
\label{fig:acc}
\end{figure*}

\subsection{Answer Extraction and Ranking}
        Once the IR engine has given us text snippets that pertain to our query, we must generate and select a good set of answers. 

\subsubsection{Answer Generation}
The 2007 CONCORDIA system guided us on our answer generation: we move through the snippets and create answers from the n-gram units that appeared \cite{razmaraconcordiauniversity}. After basic word-punctuation tokenization of each snippet, answers, a rolling 5-gram window went through each snippet; n-grams of each length 5 and below were saved as potential answers. While our initial motivation for using 5-grams as our limit derived from the CONCORDIA system, we experimented with this parameter and concluded from the graph in Figure ~\ref{fig:nlim} that 5-grams were optimal for strict scoring.  Not surprisingly, a higher selection of 9-grams achieved the highest lenient score, but we did not deem the lenient-strict trade-off to be worth the trade-off.

\begin{figure*} 
\centering
\caption{\textbf{\small \sl  Experiments with Answer Length}}


\includegraphics[width=\textwidth]{snippets.png}
\label{fig:nlim}

\end{figure*}

\subsubsection{Answer Filtering}
Even just using a unique set of every string of 5- and $<$5-grams resulted in a large number of strings resulting in an unacceptably long processing time. We took a number of steps to reasonably reduce our answer set, allowing for end-to-end running times of under four minutes, which was a welcome improvement compared to earlier 20+ minute running times. We immediately removed any answer that only occurred in one snippet. Our approach relies on the intuition that good answers will show up often, as there was no reason to keep strings that were never repeated.  The remaining answers were then put through a slight vetting process to remove redundancy.  Due to our tokenization process, it was very common to have virtually the same answer, but with an undesirable gram hanging onto it, for example the parenthesis in ``) Ted Danson" made the answer very identical to the true answer of ``Ted Danson".  The bookends of each answer were recursively checked for undesirable n-grams (punctuation and any word in the NLTK stop-word list). Any undesirable n-gram was removed and then the answer was checked again.  Only answers with no undesirable bookend grams were kept for the final answer list. \\

\subsubsection{Answer Scoring}
Basic ranking of the answers required a two-step process. First, each answer was given a base score derived by summing the text frequency of each n-gram that made it up.  The set of undesirable n-grams mentioned earlier were given a value of 0, as they were not particularly useful for finding an answer.  Additionally, words that appeared in the query itself or the question's topic were down-weighted. A handful of experiments supported our initial down-weight value estimate of 0.25, so we continued using that value and quartering the text-frequency score of any word in the query of the topic. The second step of the answer scoring was intended to select answers with words that do not only occur often, but also occur in many snippets. Each answer's score is normalized by the average Text Frequency-Inverse Document Frequency (TFIDF) score of its pieces, shown in Equation~\ref{eq:tfidf}. Undesirable grams were ignored by the calculation of average TFIDF.
\begin{equation}
S_1 = S_0 * (\frac{1}{i} * \sum\limits_{g_0}^{g_i} tf(g) *loc(\frac{Doc}{Doc(g)})
%\lable{eq:sum}
\label{eq:tfidf}
\end{equation}

\subsubsection{Guess Reweighting}
While this approach is a practical one, the top \emph{n} answers often consist of the same set of words. A naive system would end up guessing virtually the same answer repeatedly, which is not optimal.  If the answer were incorrect one guess prior, it is likely that it would still be wrong on the next guess.  Leveraging prior experience with Sum-Basic, a document-summary algorithm, we developed a re-weighting scheme based on changing the weight of n-grams that had already been presented as answers ~\cite{yih2007multi}. After each answer, our system re-weights every potential answer using Equation~\ref{eq:sum} where $\beta$  is a tunable weight and $W_c$ is the count of words the two answers share. This method cannot affect a system's accuracy, but will improve the recall of our system by expanding the scope of our guesses through the elimination of repetition. Our experiments explored using various values for $\beta$, including absolute down-weighting and found that a value between 0.7 and 0.8 gave the best results and boosted our lenient MRR between 10\% and 20\%. Our final system uses a $\beta$ of 0.75.

\begin{equation}
S_1 = S_0 * \beta ^{\sum W_c}
\label{eq:sum}
\end{equation}


\subsubsection{Answer Type Boosting}

The target answer type for each query, classified earlier, was now used to promote matching these answers.  If an answer was decided to have type-membership we applied a multiplier to its score; the greater faith we had in our membership heuristic, the greater the multiplier was. To determine if an answer was a date we looked to see if an answer either contained a month name, or had a 4-digit year in it (eg. 1642). A simple regex and a month gazeteer were employed to recognize this. The measure type was fairly uniform, it always contained a number. Checking if the answer had a number or belonged in gazeteer of common numbers as strings, we found and boosted answers that look like measures. As a precaution, answers that look like dates were not considered to belong to measure. The person type was the only other class that we could faithfully develop specific heuristics. We were able to build regexes that identified patterns that included names such as Ulysses S. Grant, F. Scott Fitzgerald or C.S. Lewis. Any answer that had these shapes got a large boost, smaller boosts were rewarded for answers in which every word was capitalized (to catch John Smith) as well as the smallest bonus for answers with at least one capital letters. The capitalization heuristic was also applied to location and organization answer types, but these answer types, as well as others, had too much variation to make good heuristics. We simply tried to find proper entities via capitalization.


\subsubsection{Answer Re-Ranking}
We employed the methodology of  Ravichandran, Hovy and Och (2003) to build a simple machine learning model to re-rank our answers. The four features were the following:
\begin{itemize}
\item Frequency:  Higher word counts are associated with the correct answer. The logarithm of the count is used as a continuous variable.
\item Expected Answer Class: An Answer Classification model was built and trained on our Question Classifier and, when they agree, this binary variable is set to 1.
\item Question Word Absent: We set this binary variable to 1, if a question word is present. Our estimate for this coefficient is negative, which is opposite the intuition of the paper.
\item Word Match: We calculate the overlap between the answer string and the question itself.
\end{itemize}
To implement this approach we first built an Answer Classifier using our predicted query classes and a simple word/POS tag learning corpus using Mallet's MaxEnt trainer.  We then were able to populate the four variables and employ Joachim's SVMrank to evaluate the re-ranking.  Unfortunately, our re-ranker did not result in an improvement in our MRR scores, so we discarded it.

\section{Answer Projection}
Our approach to discover which document within the ACQUAINT corpus the answer can be derived from is relatively naive and hinges on the hope that both the focus of the question and the answer both exist within a document. We consider each article in the AQUAINT corpus as a document and used PyLucene to index and retrieve the most relevant AQUAINT article \cite{vajda2005pulling}. We then query Lucene to search for a document that contains our answer and nothing else.  If a document is returned, we use Lucene's IR ordering to determine a source article from ACQUAINT. However, if no documents are found, we back off to a bagged search string of our answer and the question and again we defer to Lucene's returned ordering from this search.\\ 



\section{Results}
Our final results are presented below for both our development set (2006) and our evaluation set (2007). We were generally pleased with these results and note that there is a noticeable drop-off between the two. We suspect that this may be due to some of the tuning parameters, especially those in question-type boosting. Rather than using arbitrary numeric values we believe that implementing either a classifier for the answers matching a type (and not just questions matching a type) or actually tuning these weights would both benefit the system and decrease the effect of switching between datasets.


\begin{table}[ht!]
%\footnotesize
\centering
\caption{\textbf{\small \sl Summary of Results }}
\begin{tabular}{lrr}
    &  \multicolumn{2}{c}{\textbf{Year}}  \\
\cmidrule(r){2-3}

    Metric                          & 2006    & 2007    \\ 
\toprule
    Lenient                             & 0.2559  & 0.2313  \\ 
    Strict                              & 0.1256  & 0.0890  \\
    L.Accuracy                          & 18.86\% & 15.86\% \\ 
    S. Accuracy                         & 9.30\%  & 5.15\%  \\
\midrule 
 \bottomrule
 

\end{tabular}
\end{table}

\begin{figure*} 
\centering
\caption{MRR Scores across System Versions}

\label{nlim}
\includegraphics[width=\textwidth]{scores.png}
\end{figure*}


\section{Discussion}
 In our analysis of the 403 questions of the \emph{devtest}, we identified a number of shortcomings in our approach in query processing and expansion.  In particular, we realized that we had failed to properly reformulate queries with sentence-initial \textit{wh}-determiners or \textit{wh}-adverbs, with POS tags WDT and WRB, respectively.  An example of such a query might be, ``Which/WDT apple/NN is/VBZ big/JJ ?/.'' This particular issue accounted for 10 queries from the \emph{devtest} data which had produced no answer response---our system's number of no-response queries dropped from 99 to 89 upon fixing the issue. When the issue was corrected, we achieved a 4.75\% increase in lenient score, with a marginal increase in the strict score.\\

Questions starting with ``in" and ``on" also seemed problematic based on the results displayed in Table \ref{tbl:shorttable} below.\\



\begin{table*}[ht!]
\scriptsize
\centering
\caption{\textbf{\small \sl  Results versus Number of Query Expansions.}}

\begin{tabular}{lrrrrrrrrr}
      &  \multicolumn{9}{c}{\textbf{Lead Word}}  \\
\cmidrule(r){2-10}
Outcome & What & How & Who & In & Where & When & On & Which & Who \\
\toprule
 right &  16&   5&  15&   0&   4&   3&   0&   0&  15\\
 none &   5&   7&   1&  47&   1&   4&  12&  12&   1\\
 wrong & 101&  65&  38&   0&  22&  19&   0&   0&  38\\
\bottomrule
\end{tabular}
\label{tbl:longtable}
\end{table*}

This problem indicated that our baseline system's query processing module did not properly process queries with sentence-initial prepositions. To address the baseline system's shortcoming, we added some query expansion logic in order to correctly reformulate queries beginning with a preposition (POS tag: IN) followed by a \textit{wh}-pronoun (WP) or \textit{wh}-determiner (WDT). An example of such a query might be, ``In/IN which/WDT country/NN is/VBZ Paris/NNP ?/.'' After augmenting our query expansion logic, our system's number of no-response queries dropped from 89 to 22, and our MRR scores increased to 0.2106 (lenient) and 0.0569 (strict) (some of the increase in MRR was due to unrelated parameter tweaks).\\

Clearly, this prior inadequacy of our baseline system accounted for the bulk of unanswered queries. However, we worked to reduce the number of unanswered queries further. We determined that our query expander was yielding no expansions for 18 different queries drawn from the \textit{devtest} dataset. To address this problem, we opted for the very simple solution of implementing a back-off feature in our query expansion module. In the event that a query given to the expansion module did not match any of the module's expansion or reformulation heuristics, the module's back-off feature would simply pass the unchanged query straight to the web search module. The main idea behind this approach was the intuition that passing the original query to the web search module was clearly preferable to using an empty string as a search term. As a result of this change, our system's number of no-response queries dropped by 17 queries, from 22 to 5, while our MRR scores increased to 0.2342 (lenient) and 0.0614 (strict). Future work on the system would involve devising a more sophisticated method of creating expansions and/or reformulations for the 18 queries which yielded no expansions before the addition of our simple back-off feature.\\

We were initially unable to come up with a solution to the remaining five queries which returned no answer response from our system. The queries are listed below:\\

\noindent
\textbf{170.2} \emph{What John Prine song was a \#1 hit for George Strait?}\\
\textbf{172.4} \emph{What is Jerry's last name?}\\
\textbf{172.5} \emph{What rock band had a Ben \& Jerry's flavor named after them?}\\
\textbf{172.6} \emph{Unilever purchased Ben \& Jerry's in 2000 for what price?}\\
\textbf{215.6} \emph{Which film won three awards at the festival?}\\

Only one of the above five queries (\textbf{172.6}) is among those for which our query expansion module had been unable to produce any expansions or reformulations. The other four queries had been properly reformulated by the expansion module without the need for resorting to its back-off feature, but each such query only yielded a single reformulation. After revisiting this problem, we discovered that queries \textbf{170.2}, \textbf{172.5}, and \textbf{172.6} were not being properly URL-encoded by the third-party BingSearchAPI Python module we were using. Once we fixed the URL-encoding, the aforementioned three queries started returning results, but did not improve system performance after all. As for remaining two queries (\textbf{172.4} and \textbf{215.6}), their failure to return any answer might be an indication that some of our query reformulation heuristics require revision, possibly with the goal of increasing the number of reformulations yielded per query input to the query expander. Table \ref{tbl:ashorttable} shows that the ratio of questions with an outcome of \textit{none} to questions with an outcome of either \textit{right} or \textit{wrong} (and the same number of expansions) is much higher for questions with only one expansion than for questions with more than one.


\begin{table}[ht!]
\scriptsize
\centering
\caption{Results versus Number of Query Expansions. }
\begin{tabular}{lrrrrrrrrr}
     &  \multicolumn{9}{c}{\textbf{Number of Expansions}}  \\
\cmidrule(r){2-10}
Outcome & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9\\
\toprule
 right &  32&   4&   0&   0&   0&   4&   3&   0&   0\\
 none & 104&   6&   0&   0&   0&   1&   4&   0&   0\\
 wrong & 163&  41&   0&   0&   0&  22&  19&   0&   0\\
 \bottomrule
\end{tabular}
\label{tbl:ashorttable}
\end{table}


\normalsize
    
  

\section{Conclusion}
While we are pleased with the overall results of our system built over a relatively short seven week period of time, we have noted a number of areas that could be improved.  As mentioned earlier, we do rely on weights and tuning weights, which may not be the best way to approach a system like this.  As we only tested and tuned one weight at a time, the co-effects of changing weights were not measured. Various genetic tuning algorithms exist, which allow for iterative training of weights that adjust multiple weights each generation.  In future work we would opt toward implementing one of these, if we continued working with numeric weights.  The other option would be to use classifiers instead of weights.  We attempted some of this with classifier-based re-ranking.  Unfortunately this experiment did not produce a meaningful improvement, but this angle of attack is one that should be explored further, even if it requires building training data that we did not have while creating the system discussed above.


\bibliographystyle{acl}
\bibliography{bibliography}

\end{document}