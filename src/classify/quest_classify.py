#!/usr/bin/env python2.7

# question classification
# Nick Waltner

import re,sys,nltk
import subprocess
import os
#num = int(sys.argv[1])
#print num

src = os.path.dirname(os.path.abspath(__file__))
model = os.path.join(src, 'model')
temp_file = os.path.join( src,'tmp')
mallet_results = ''

best_labels = []

def classify(quest) :
    '''
    quest = open("2004.txt")
    quest = open("questions.2006.txt")
    #quest = open("2005.txt")
    f = open('data.out', 'w')
    '''

# ensure there is a dir
    
#    nn = 1
    fnew = open(temp_file, 'w')
    for query in quest:
       #token = nltk.tokenize.word_tokenize(sentence)
#       (a,b,type,category,quest) = question.split(":")
#       query = quest.rstrip()
       #print num,query
       query = nltk.pos_tag(nltk.word_tokenize(query))
       fnew.write(' '.join(t[0] + ' ' + t[1] for t in query) + '\n')
#       name = "quest." + "." + str(nn)
#           f.write(temp)

#       nn = nn + 1
    fnew.close()
    # mallet call
    mallet_results = filter(None, subprocess.check_output(['mallet', 'classify-file', '--input', temp_file, '--classifier', model, '--output', '-']).split('\n'))
    # Run through each result, get the best label, append it
    for r in mallet_results :
        label = process_result(r)
        best_labels.append(label)
    # clean-up of the tmp file that mallet requires goes here
    os.remove(temp_file)
    # give back a list, same order as it came in with the labels
    return best_labels

'''This function will calcuate the highest score for a mallet return :'''
def process_result(s) :
    #returns the label with the highest value from a MALLET result string
    s = s.split()
    scores = {}
    for i in xrange(1,len(s),2) :
        label = s[i]
        value = float(s[i+1])
        scores[label] = value
    return sorted(scores, key=scores.get, reverse=True)[0]
    
    
