'''
    Central location for regexes or other data pertaining to filtering
    Store type regexes or entity structures here.
'''
import re
from sets import Set
import nltk

def is_place(answer) :
    return is_organization(answer)

def is_organization(answer) :
    """
       Determine a multiplier for organization-looking things
       :return float: multiplier
       :param answer: string of possible answer type
    """
    if len(allCaps.findall(answer)) == 1 :
        return 2
    elif len(anyCaps.findall(answer)) :
        return 1.75
    else :
        return 1

def is_name(answer) :
    """
       Determine a multiplier for name-like things
       :return float: multiplier
       :param answer: string of possible answer type
    """
    if full_name.findall(answer) or penname.findall(answer) :
        # Rather certain these patterns are names
        return 2.0
    elif len(allCaps.findall(answer)) == 1 :
        # less certain, and so forth
        return 1.75
    elif len(anyCaps.findall(answer)) :
        return 1.5
    else :
        return 1
    

# Match stuff like Ulysses S. Grant
full_name = re.compile('[A-Z][a-z]+ [A-Z]\.? [A-Z][a-z]+')
# Match stuff like J.R.R. Tolkein
penname = re.compile('([A-Z]\.)+\.? ([A-Z][a-z]* ?)+')
allCaps = re.compile('[A-Z][a-zA-Z]*( [A-Z][a-zA-Z]*)*')
anyCaps = re.compile('[A-Z]')

def is_measurement(answer) :
    """ :return float: 2.0 :param answer: looks like a measurement, 1.0 otherwise """
    tokens = answer.split(' ')
    measurement = False
    for gram in tokens: 
        if gram.lower() in months :
            # numbers + month is more likely a date than a measurement
            return 1.0
        elif gram.lower() in numbers :
            measurements = True
        else :
            try :
                float(gram)
                measurements = True
            except ValueError :
                pass
    if measurement :
        return 2.0
    return 1.0
        
numbers = Set(['zero','one','two','three','four','five','six','seven','eight','nine','ten',
    'eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen',
    'twenty','thirty','forty','fifty','sixty','seventy','eighty','ninety','hundred',
    'first','second','third','fourteen','fifteen','sixth','seventh','eight','ninth','tenth'])
    

def is_date(answer) :
    """ :return float: 2 if :param answer: looks like a date, 1 otherwise """
    tokens = answer.split(' ')
    for gram in tokens :
        if gram.lower() in months :
            return 2.0
    if year.match(tokens[-1]) :
        return 2.0
    return 1.0
        

months = Set(['jan','feb','mar','apr','may','jun','jul','aug','sept','oct','nov','dec',
    'jan.','feb.','mar.','apr.','may.','jun.','jul.','aug.','sept.','oct.','nov.','dec.',
    'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'october','november','december'])
    
year = re.compile('[0-9]{4}s?')

def resolve_references(query=None, focus=None) :
    # order is important here
    # try to make non-specific entities into more specific entities
    # break the quasi-phasal focii into quasi NERS ie. 'Thabo Mbeki elected president' -> 'Thabo Mbeki'
    focus = first_verb(focus)
    if focus not in query :
        #last word of the quasi NER will generally be the reference ex. Moon for Warron moon, division for 82nd Airborn Division
        unspecific = [focus.split(' ')[-1], focus.split(' ')[-1].lower()]
        for word in unspecific :
            if word in query :
                query = query.replace(' ' + word, ' ' + focus)
                break
        #run through the 
        # we don't want to replace if the whole thing is in there
        # The American Institute should not pass in for "Institue" if the Q is "Where is the American Institute"
    for word in query.split() :
        if word in subj_pronouns :
            if word == 'it' or word =='It':
                focus = 'the ' + focus
            if word == 'his' or word == 'His' or word =='her' or word == 'Her':
                focus += "'s"
            query = query.replace(' ' + word, ' ' + focus)
    return query

def first_verb(phrase) :
    """ 
        returns :param phrase: up until the first verb
        intended as a cheap NP chunker for TREC question headers
    """
    if "'" in phrase:
        return phrase.split(' ')[0]
    tagged = [(word, nltk.tag.simplify.simplify_wsj_tag(tag)) for word, tag in nltk.pos_tag(phrase.split(' '))]
    for i in xrange(1,len(tagged)) :
        tag = tagged[i][1]
        if tag == 'V' or tag == 'VD' or tag == 'VBG' or tag == 'VBZ':
            return ' '.join([p[0] for p in tagged[:i]])
    return phrase
    
subj_pronouns = Set(['I','we','she','She','his','His','he','her','they','it','We','Her','They','It','He'])