#!/opt/python-2.7/bin/python2.7

'''
    @author Tristan Bodding-Long
        IR corpus managing TF/IDFs for the documents held in it
'''
import nltk
from math import log
import sgmllib
import os
from sets import Set

##########
#
# GLOBAL
# In accordance with Concordia, start the answer limit at 5-grams
n = 5


class Corpus :
    '''
        Contain the set of documents to retrieve from.
        Also manages word-document frequency to decide best document
    '''
    
    def __init__(self) :
        self.df = {}
        self.freq = {}
        self.docs = []
        self.answers = Set([])
        
    def calc_tfidf(self, word) :
        N = len(self.docs)
        # get the counts
        try :
            tf = self.freq[word]
        except KeyError:
            tf = 1
        try :
            df = self.df[word]
        except KeyError :
            # never seen make the result impossibly small
            df = N
        return tf * log(N/df)
     

    #   Depricated Method
    def find_best_doc(self, words) :
        best_score = 0
        best_doc = None
        for doc in self.docs :
            score = 0
            for word in words :
                score += self.calf_tfidf(word, doc)
            if score > best_score :
                best_doc = doc
        return best_doc.name

        
    def add_text(self, text, name=None) :
        ''' used to create a document in the corpus from raw text'''
        # make a new document
        self.docs.append(self.Document(text))
        # update counts of words seen per 
        lastdoc = self.docs[-1]
        self.answers = self.answers.union(lastdoc.answers)
        for key in lastdoc.tf :
            if key in self.df :
                self.df[key] += 1
                self.freq[key] += lastdoc.tf[key]
            else :
                self.df[key] = 1
                self.freq[key] = self.docs[-1].tf[key]
        
    
    def add_sgml_file(self, path) :
        ''' used to create a document given a path to an sgml-formatted file'''
        text = [l.strip() for l in open(path).readlines()]
        TE = Text_Extractor()
        # extract just the text fields from the SGML & call to make a new doc
        TE.feed(' '.join(text))
        self.add_text(TE.text, name=path.split(os.sep)[-1])
    
    def parse_sgml_file(self, path) :
        ''' Returns a list of text fields in sgml '''
        text = [l.strip() for l in open(path).readlines()]
        TE = Text_Extractor()
        # extract just the text fields from the SGML & call to make a new doc
        TE.feed(' '.join(text))
        return TE
        
        
    class Document:
        '''
        hold each document's words, names & counts
        '''
   
    
        def __init__(self, text, name=None) :
            global n
            stopwords = nltk.corpus.stopwords.words('english')
            # read in document as pure text
            if name is not None :
                self.name = name
            self.tf = {}
            self.answers = Set([])
            # get rid of '...'
            text = filter(None, text.split('...'))
            for chunk in text :
                words = nltk.tokenize.word_tokenize(chunk)
                for i in xrange(len(words)) :
                    for j in xrange(n,0,-1) :
                        if i + j <= len(words) :
                            # pulls out all possing 5- grams as an 'answer'
                            phrase = ' '.join(words[i:i+j])
                            if phrase.lower() not in stopwords :
                                self.answers.add(phrase)
                                #pulls in counts of each phrase
                                if phrase in self.tf :
                                    self.tf[phrase] += 1
                                else :
                                    self.tf[phrase] = 1
                        
class Text_Extractor(sgmllib.SGMLParser):
    '''Means of retrieving text from the ACQUAINT corpus'''

    def __init__(self, verbose=0):
        sgmllib.SGMLParser.__init__(self, verbose)
        self.text = []
        self.docno = []
        self.data = None

    def handle_data(self, data):
        if self.data is not None:
            self.data.append(data)
            
    def start_text(self, attrs):
        self.data = []

    def end_text(self):
        self.text.append("".join(self.data))
        self.data = None
        
    def start_docno(self, attrs):
        self.data = []
     
    def end_docno(self) :
        self.docno.append(''.join(self.data))
        self.data = None
