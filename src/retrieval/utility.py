#!/opt/python-2.7/bin/python2.7
'''
    @author Tristan Bodding-Long
    File of helper functions that don't really fit elsewhere
'''
import os

def full_listdir(d):
    # returns full path of everything in a directory
    return [os.path.join(d, f) for f in os.listdir(d)]