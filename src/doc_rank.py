#!/opt/python-2.7/bin/python2.7
'''
    @author Tristan Bodding-Long
    intended to rank the acquaint documents given query/answer pairs
'''
import xml.etree.ElementTree as etree
import os, re, sys, nltk
import string
from sys import stderr
from retrieval import utility, corpus
from time import time
import lucene
from lucene import \
    SimpleFSDirectory, System, File, \
    Document, Field, StandardAnalyzer, IndexWriter, IndexSearcher, Version, QueryParser

def is_ascii(c):
    return ord(c) < 128 and ord(c) >= 0

def to_ascii(s):
    return filter(is_ascii, s)

class XMLExtractor:
    """
    Class for extracting id attribute and text from all DOC elements 
    in XML file's parse tree
    """
    def __init__(self, path):
        self.docno = []
        self.text = []
        tree = etree.parse(path)
        docs = tree.findall(".//DOC")
        for doc in docs:
            docno = doc.attrib["id"]
            text = " ".join(paragraph.text.strip() for paragraph in doc.findall(".//TEXT/P"))
            self.docno.append(docno)
            self.text.append(text)

def parse_xml_file(self, path):
    """
    Returns an object with text and docno attributes
    """
    tree = etree.parse(path)
    docs = tree.findall('.//DOC')

class Ranker :
    '''
        control the automation of loading the lucene index of ACQUAINT
        process document scores based on answers
    '''
    
    def __init__(self, path='/corpora/LDC/LDC02T31') :
        self.corpus = corpus.Corpus()
        self.lucene_special = re.compile('[\\\+!\(\)\[:\^"\]\{\}~\*\?]')
        lucene.initVM()
        self.lucene_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "ACQUAINT")
        # see if acquaint needs to be populated
        self.dir = SimpleFSDirectory(File(self.lucene_dir))
        self.analyzer = StandardAnalyzer(Version.LUCENE_30)
        if not os.path.exists(self.lucene_dir):
            writer = IndexWriter(self.dir, self.analyzer, True, IndexWriter.MaxFieldLength(512))
            # patas path to ACQUAINT
            ACQUAINT_path = path
            s_time = time()
            files_added = 0
            print 'beginning loading corpus'
            # Only files that we want are in the folders apw/nyc/xie
            file_stack = []
            for file in utility.full_listdir(ACQUAINT_path) :
                if os.path.isdir(file) :
                    file_stack.append(file)
            # recurse through the targetted directories
            while file_stack :
                current_file = file_stack.pop()
                if os.path.isdir(current_file) :
                    file_stack.extend(utility.full_listdir(current_file))
                else :
                    files_added += 1
                    # Extractor has fields for docno (aka document id) & text
                    # make XML extractor if .xml extension found, else SGML
#                    print >> sys.stderr, os.path.splitext(current_file)[1].lower()
                    if os.path.splitext(current_file)[1].lower() == ".xml":
                        Extractor = XMLExtractor(current_file)
                    elif os.path.splitext(current_file)[1].lower() == ".dtd":
                        pass
                    else:
                        Extractor = self.corpus.parse_sgml_file(current_file)
                    for i in xrange(len(Extractor.text)) :
                        doc = Document()
                        doc.add(Field('name', Extractor.docno[i], Field.Store.YES, Field.Index.ANALYZED))
                        doc.add(Field('text', Extractor.text[i], Field.Store.YES, Field.Index.ANALYZED))
                        writer.addDocument(doc)
                    sys.stderr.write(str(files_added) +  ': ' + current_file + '\n')
            writer.optimize()
            writer.close()
            f_time = time()
        #corpus is loaded for Lucene
        self.searcher = IndexSearcher(self.dir)
        
    def get_best_document(self, query='', answer='') :
        ''' given a query & an answer tries to find the best ACQUAINT doc supporting it'''
        # escape special characters in Q & A
        # hyphens are weirder, just treat them as seperate words
        # dropping extended characters
        query = re.sub('-', ' ', ''.join(c for c in query if c in string.printable).strip())
        answer = re.sub('-', ' ', ''.join(c for c in answer if c in string.printable).strip())
        answer = re.sub(self.lucene_special, '', answer)
        query = re.sub(self.lucene_special, '', query)
        # Set of fallbacks to look for if the exact answer string isn't in Lucene
        bag = [re.sub('"', '', query), re.sub('"', '', answer)]
        searches = ['+"' + answer + '" ' + ' '.join(bag), answer + ' ' + query ]
        for term in searches :
            search_query = QueryParser(Version.LUCENE_30,'text', self.analyzer).parse(term)
            hits = self.searcher.search(search_query, 1)
            if len(hits.scoreDocs) >= 1 :
                #try to ensure there is a hit
                return self.searcher.doc(hits.scoreDocs[0].doc).get('name')
        return 'Nil'
        '''
        # Lucene set-up
        search_term = '"' + '" "'.join(bag) + '"~5'
        with open('crap.txt', 'a') as crapfile:
            print >>crapfile, search_term
        search_query = QueryParser(Version.LUCENE_30,'text', self.analyzer).parse(search_term)
        # arbitrary no. for now
        hits = self.searcher.search(search_query, 20)
        if len(hits.scoreDocs) > 1 :
            #try to ensure there is a hit
            doc = self.searcher.doc(hits.scoreDocs[0].doc)
        else :
            # fall back onto back of words
            search_query = QueryParser(Version.LUCENE_30,'text', self.analyzer).parse(' '.join(query + answer))
            hits = self.searcher.search(search_query, 20)
            if len(hits.scoreDocs) > 1 :
                doc = self.searcher.doc(hits.scoreDocs[0].doc)
            else :
                # fall further back onto giving up
                return 'Nil'
        #note the score of the document
#        sys.stderr.write('Runner-up: ' + searcher.doc(hits.scoreDocs[1].doc).get('name') + ' third-best: ' + searcher.doc(hits.scoreDocs[2].doc).get('name') + '\n')
        return doc.get('name')
'''
    def best_n(self, input, n=10):
        """ 
            :return list: text of the documents that best match the best n documents 
            :param input: string to be matched against the corpus
            :param n: num of documents to return
        """
        print input
        articles = []
        search_term = '"' + input + '"'
        search_query = QueryParser(Version.LUCENE_30,'text', self.analyzer).parse(search_term)
        hits = self.searcher.search(search_query, n)
        for hit in hits.scoreDocs:
            articles.append(self.searcher.doc(hit.doc).get('text'))
        if len(articles) < n :
            search_query = QueryParser(Version.LUCENE_30,'text', self.analyzer).parse(re.sub(self.lucene_special, '',input))
            hits = self.searcher.search(search_query, n)
            for hit in hits.scoreDocs:
                articles.append(self.searcher.doc(hit.doc).get('text'))
        return articles[:n]
