'''
    @author Tristan Bodding-Long
    take a list of 'snippets'  generated from some search query
    attempt to find the n-gram with the most overlap from these pieces
'''

from retrieval import corpus, filters
import nltk
import operator
from re import match, compile
from sets import Set
from sys import stderr



stopwords = Set(nltk.corpus.stopwords.words('english'))
word_patt = compile('[\w]+')
nonword_patt = compile('[^\w]+')




class Ranker():
    """
        Class to utilize tf and idf to choose the best sequence from given docs
    """
    
    def __init__(self, query, snippets, target = None) :
        """
            :param query: string containing words to downweight
            :param snippets: list of strings
            :param target: string-- answer type, this provides variable weighting to answers based on word shape
        """
        self.query = Set([w.lower() for w in nltk.tokenize.word_tokenize(query)])
        self.snip_corpus = corpus.Corpus()
        for snip in snippets :
            # get counts for everything in the snippets
            self.snip_corpus.add_text(snip)
        self.answers = {a:1 for  a in self.snip_corpus.answers}
        # prune answers that don't follow a type paradigm
        self.type_filter()
        # score answers based on \sigma unigram frequencies
        self.unigram_score()
        # rescore answers again based on \delta tfidf
        self.tfidf_score()
        # rescore answers again based on the answer looking correct/incorrect
        if target :
            self.rescore(target)
            
    def rescore(self, target) :
        """
            Depending on the value of target, calls to check the answer by shape or other heuristics
        """
        target = target.lower()
        if target == 'person':
            fx = filters.is_name
        elif target == 'date' :
            fx = filters.is_date
        elif target == 'measure' :
            fx = filters.is_measurement
        elif target == 'organization' :
            fx = filters.is_organization
        elif target == 'place':
            fx = filters.is_place
        else:
            return
        for candidate in self.answers :
            self.answers[candidate] *= fx(candidate)
        
    def get_best(self, n) :
        """
            returns the n answers with the best score
            Use reweighting derived from SumBasic
        """
        top_answers = []
        while len(top_answers) < n and len(self.answers) > 0 :
            best = sorted(self.answers.iteritems(), key= operator.itemgetter(1), reverse=True)[0][0]
            top_answers.append(best)
            del self.answers[best]
            # reweight answers based on the words in our chosen answers
            words = Set([w for w in best.split(' ')])
            for answer in self.answers :
                contained = 0
                # check to see if the word is scorable
                # and if the word in is our candidate answer, note if so
                for word in words :
                    if word.lower() not in stopwords and word_patt.match(word) :
                        if word in answer :
                            contained += 1
                self.answers[answer] = self.answers[answer]* (.75**contained)
        return top_answers
            
    def get_all(self) :
        """
            Debugging method
            No reweighting based on seen answers
        """
        l = [a for a in sorted(self.answers.iteritems(), key=operator.itemgetter(1), reverse=True)]
        return l        
        
    def tfidf_score(self) :
        """
            reform each score based on the average tfidf of its pieces
            DO NOT call before some scoring method 
        """
        for candidate in self.answers :
            l = 0
            tfidf = 0
            for word in candidate.split(' ') :
                if word.lower() not in stopwords and word_patt.match(word) :
                    l += 1
                    tfidf += self.snip_corpus.calc_tfidf(word)
            # new score based on average tfidf of pieces
            if tfidf > 0 and l != 0 :
                self.answers[candidate] = self.answers[candidate] * (tfidf / l)
            else :
                self.answers[candidate] = 0
            
    def unigram_score(self) :
        """
            Value each answer based on the sum of their parts in unigra TF value
        """
        for candidate in self.answers :
            score = 0
            for word in candidate.split(' ') : 
                if (word.lower() not in stopwords) and word_patt.match(word) :
                    # Heavily way words in the question down down
                    if word.lower() in self.query :
                        weight = .25
                    else :
                        weight = 1.0
                    # access the No. of times a word occurred
                    score += (self.snip_corpus.freq[word] * weight)
            self.answers[candidate] = score
        
    def type_filter(self) :
        """ 
            chuck out low-occurrence strings
                can call various other method techniques to vet the potential answer
        """
        valid = {}
        deletions = Set([])
        for candidate in self.answers :
            # remove answers that only occur in 2 or fewer docs
            if self.snip_corpus.df[candidate] >= 2 :
                answer = valid_answer(candidate)
                if answer :
                    valid[answer] = 0            
        self.answers = valid
        
def valid_answer(answer):
    """ 
        Ensures that an answer cannot be:
            be made from all 0-val tokens
            cannot start or end with 0-val tokens
        :param string:  potential answer
        :return string: good answer based on above conditions
        :return None: if no good answer can be derived
    """
    grams = answer.split(' ')
    # peel off the front
    while grams[0].lower() in stopwords or nonword_patt.match(grams[0]) :
        grams = grams[1:]
        if not grams:
            return None
    # peel off the back
    while grams[-1].lower() in stopwords or nonword_patt.match(grams[-1]) :
        grams = grams[:-1]
        if not grams:
            return None
    return ' '.join(grams)