import QA_system as qa
from retrieval import filters
import query_expander as qe

path = '/dropbox/12-13/573/Data/Questions/devtest/TREC-2006.xml'


queries = qa.divine_queries(path)
expander = qe.Expander()

misses = 0

for i in xrange(len(queries)) :
    q_tuple = queries[i]
    # flesh out the tuple into its pieces
    subID = q_tuple[0]
    query = q_tuple[1]
    focus = q_tuple[2]
    query = filters.resolve_references(query=query, focus=focus)
    # Python Port goes here
    searches = expander.expand(query)
    # do w/e with the searches

    if not searches:
        print query
        misses += 1

print "Missed {0} queries".format(misses)
