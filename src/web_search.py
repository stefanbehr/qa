'''
    Module for sending query strings to web search API 
    and returning web snippets from search results
'''

from bing_search_api import BingSearchAPI
from sys import stderr
import os, time, urllib2
try:
	import cPickle as pickle
except ImportError:
	import pickle

# redundant if saving cache to file and reading back in
# def memoize(f):
# 	"""
# 	Decorator function for memoizing functions.
# 	"""
# 	cache = {}
# 	def memoized(*args, **kwargs):
# 		# hashable version of args
# 		h_args = tuple(args)
# 		# hashable version of kwargs
# 		h_kwargs = tuple(sorted("{0}={1}".format(str(key), str(value)) for key, value in kwargs.items()))
# 		all_args = h_args + h_kwargs
# 		if all_args not in cache:
# 			cache[all_args] = f(*args, **kwargs)
# 		return cache[all_args]
# 	memoized.__doc__ = """
# 	Memoized version of function <{0}>
# 	Docstring of original function: 
# 	{1}""".format(f.__name__, f.__doc__)
# 	memoized.__name__ = f.__name__
# 	memoized.cache = cache
# 	return memoized

class SearchEngine:
	"""
	Search engine class
	"""

	def __init__(self, keyfilename=".bingkeys", response_format="json", num_results=50, skip=0, cachefilename=".searchcache"):
		"""
		Initialize SearchEngine object with available API keys.

			:param keypath: Path to file storing API keys, .bingkeys by default
			:param response_format: Format of API response object, JSON by default
			:param num_results: Number of search results to acquire per API request, 50 by default (API limits to 50)
			:param skip: Number of results to skip (?), 0 by default
			:param cachepath: Path to cache containing pickled dict data struct
		"""

		src_dir = os.path.dirname(os.path.abspath(__file__))
		keypath = os.path.join(src_dir, keyfilename)
		cachepath = os.path.join(src_dir, cachefilename)

		# read in available keys
		with open(keypath) as keyfile:
			self.keys = keyfile.read().strip().split("\n")

		if len(self.keys) < 1:
			raise Exception("No API keys found")

		# set param dict for passing to API via wrapper
		self.params = {
						"$format": response_format,
						"$top": num_results,
						"$skip": skip
		}

		self.search_type = 'web'
		self.cachepath = cachepath

		# set current api_key
		self.api_key = self.keys.pop()
		# create API object
		self.bing = BingSearchAPI(self.api_key)

		# load cached queries/results or create empty cache
		try:
			cachefile = open(cachepath)
		except IOError:
			self.cache = {}
		else:
			self.cache = pickle.load(cachefile)
			cachefile.close()

	def search(self, query):
		"""
		Send query to search engine API and return list of 
		search result snippets

			:param query: Search string
		"""
		if query not in self.cache:
			snippets = []
                        safe_query = urllib2.quote(query)
			try:
				response = self.bing.search(self.search_type, safe_query, self.params)()
			except:
				# catch-all, probably JSON encode error (might no longer be necessary)
				time.sleep(2.5)
				try:
					response = self.bing.search(self.search_type, safe_query, self.params)()
				except:
					# still something wrong, give up
					print >>stderr, "query: {0}".format(repr(query))
					self.cache[query] = snippets
					return self.cache[query]
			results = response["d"]["results"][0]["Web"]
                        snippets = [result["Description"].encode("utf8") for result in results]
			self.cache[query] = snippets
		return self.cache[query]

	def __del__(self):
		"""
		Store data in self.cache to file at self.cachepath
		"""
		with open(self.cachepath, "w") as cachefile:
			pickle.dump(self.cache, cachefile)
