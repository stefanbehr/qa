'''
    Top level framework for a QA system minimally compatible with TREC 2003-2006 
    Manage IO
    Tasking modules
 '''
 
from optparse import OptionParser
from sys import argv, exit, stderr
import os, codecs
import xml.etree.ElementTree as ET
import subprocess
import itertools
from time import time
####
# Imports from src folder
#
import query_expander as qe
import web_search as ws
import shallow_ranker
import doc_rank
from retrieval import filters
from classify import quest_classify as qc

# imports for debugging
import types
from lucene import JavaError

def divine_queries(input_path) :
    """
        pulls out the queries from a TREC XML file
        :param input_path: full file path to the XML
        :returns: list [(id, query, focus)]
        
        Will kill the program on a bad file
    """
    try :
        root = ET.parse(input_path).getroot()
    except ET.ParseError :
        print 'Failed to read XML from file ' + question_path
        exit(1)
    # find all Factoid queries
    factoid_queries = []
    for question in root :
        focus = question.get('text')
        for child in question :
            # Subid in X.Y format
            q_element = child.getchildren()[0]
            query = q_element.text.strip()
            subID = q_element.get('id')
            type = q_element.get('type')
            if type == 'FACTOID' :
                # a question type we want, keep it
                factoid_queries.append( (subID, query, focus) )
    return factoid_queries

def main() :
    # All input commands go here
    parser = OptionParser(usage = 'usage: %prog [options]')
   
   
    parser.add_option("-a", "--input-answers", type='string', dest="pattern_path",
    help="Path of TREC pattern file")
    parser.add_option("-e", "--evaluate-strict", action='store_true', dest='scoring_flag', default=False,
    help="Run system with a strict evaluatione, default False/lenient")
    parser.add_option("-c", '--corpora', type='string', dest='corp_path', default=None,
    help="Tells the system where to find an ACQUAINT corpus, if none exists")
    parser.add_option("-n", dest='max_results', type='int', default=20,
    help="Number of answers to output for each question, default = 20")
    parser.add_option("-o", "--output", type='string', dest="output_path", default=os.getcwd() + os.sep + 'output.txt',
    help="Path to write answers, default ./output.txt")
    parser.add_option("-q", "--input-questions", type='string', dest="question_path",
    help="Path of TREC-formatted Question file")
    parser.add_option("-r", dest='runtag', type='string', default='UNKRUN',
    help="Provides the means to label a single run of the system")
    # if we want to tune turning on off some processing, can do so here
    #
    # Boolean ex:
    # parser.add_option("-w", "--wordnet", action="store_true", dest="WNFlag", default=False,
    # help="flag to note control WORDNET expansion on Query Processing")
    #load options
    (options, args) = parser.parse_args()
    # ensure the minimum required doc paths are known
    #if not ( os.path.isfile(options.question_path) and os.path.isfile(options.pattern_path)) :
    #    parser.error("QASystem.py requires question and answer paths")
    # Main automation
    # Load in TREC question XML
    queries = divine_queries(options.question_path)
    # get the type of answer each question is looking for :
    types = qc.classify([s[1] for s in queries])
    #Queries are returned in the form [(full id, query, focus)]
    # Set-up the output writer
    with open(options.output_path, 'w') as output_writer :
        # Instantiation of the component modules
        ranker = doc_rank.Ranker(path=options.corp_path)        
        expander = qe.Expander()
        search_engine = ws.SearchEngine()
        # iterate through each factoid query
        t = Timer()
        for i in xrange(len(queries)):
            t.reset()
            q_tuple = queries[i]
            # flesh out the tuple into its pieces
            subID = q_tuple[0]
            query = q_tuple[1]
            focus = q_tuple[2]
            #Change the make-up of queries to remove pronouns & inexact references
            query = filters.resolve_references(query=query, focus=focus)
            # Expect a tuple back from query parse(String, [Strings]) == (What kind of answer we're looking for, [different searches])
            searches = expander.expand(query)
            t.note()
            # just use plain query if no expansions found
            if not searches:
                searches = [query]
            # pass the parses into our webSearcher piece by piece and collect the answers
            web_results = []
            for s in searches :
                if web_results:
                    web_results = list(itertools.chain(*itertools.izip(web_results, search_engine.search(s))))
                else:
                    web_results = search_engine.search(s)
            t.note()
            # Snippets are all generated!
            # Send the results in to the shallow ranker, can limit how many results we get back to reduce the stress on deeper processes
            sRanker = shallow_ranker.Ranker(query + ' ' + focus, web_results[:65], target=types[i])
            t.note()
            for answer in sRanker.get_best(options.max_results) :
                # ranker wants the raw query & full answer to try and find a document that links the two
                try:
                    document = ranker.get_best_document(query=focus, answer=answer)
                except JavaError:
                    # handle weird plain javaerror and continue looping
                    print >>stderr, repr(focus)
                    print >>stderr, repr(answer)
                    continue
                output_writer.write(str(subID) + ' ' + options.runtag + ' ' +  document.encode('utf8') + ' ' + answer + '\n')
            t.note()
    t.report()
    ###
    # 
    # Code pertaining to scoring the document
    # lenient & strict
    year = options.output_path.split(os.sep)[-1]
    subprocess.call(['python2.6', '/opt/dropbox/12-13/573/code/compute_mrr_rev.py', options.pattern_path, options.output_path, 'lenient'], stdout=open('results/' + year + '_lenient', 'w'))  
    subprocess.call(['python2.6', '/opt/dropbox/12-13/573/code/compute_mrr_rev.py', options.pattern_path, options.output_path, 'strict'], stdout=open('results/' + year + '_strict', 'w'))

class Timer() :
    """ small timer class for figuring out what % of time is consumed by what module """
    
    def __init__(self) :
        self.time = time()
        self.node = 0
        self.counts = []
        
    def reset(self) :
        self.time = time()
        self.node = 0
     
    def note(self) :
        try :
            self.counts[self.node] += time() - self.time
        except IndexError:
            self.counts.append(time() - self.time)
        self.node += 1
        self.time = time()
    
    def report(self):
        total = sum(self.counts)
        percent = [str(100 * (l/total))[:4] + '%' for l in self.counts]
        print >> stderr, '\t'.join(['Query', 'Web', 'Answers', 'Lucene'])
        print >> stderr, 's\t'.join([str(o).split('.')[0] for o in self.counts])
        print >> stderr, '\t'.join(percent)
    
    
if __name__ == "__main__":
    main()
