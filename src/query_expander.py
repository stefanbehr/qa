# Ported from Perl by Stefan Behr
# Perl originally written by Nick Waltner

import nltk, re

class Expander:
    """
    Class for expanding queries
    
    Create new Expander object: Expander()
    Expand query: Expander().expand(<query>)
    """
    def __init__(self):
        self.wh_classes = {"where": ["located", "situated", "in", "near", "by"],
                            "when": ["year", "date", "time", "day", "on", "in"]}
        self.distance_words = ["miles", "kilometers", "days", "hours", "minutes",
                                "seconds", "yards", "meters", "feet", "inches"]
        self.final_punc_tags = [":", ".", "''"]
        self.verb_tags = ["VBZ", "VBP", "VBD"]
        # save list of references to object's bound methods 
        # whose names start with "expand_"
        self.expansion_methods = [getattr(self, attr) for attr in dir(self) if attr.startswith("expand_")]

    def expand(self, query):
        """
        Given a query string, returns a list of query 
        expansion strings.
        """
        expansions = []
        
        # tokenize, pos tag
        word_tag_pairs = nltk.pos_tag(nltk.word_tokenize(query))
        # remove query-final punctuation
        word_tag_pairs = self.strip_final_punc(word_tag_pairs)
        
        # create and accumulate all expansions
        for expansion_method in self.expansion_methods:
            try:
                expansions.extend(expansion_method(word_tag_pairs))
            # in the unlikely event that the query is too short,
            # this will save us
            except (IndexError, ValueError):
                continue
        
        # remove duplicate expansions
        expansions = list(set(expansions))
        # post_process expansions (s/ 's/'s/g)
        expansions = map(self.fix_possessives, expansions)
        
        return expansions

    def expand_prepositions(self, wt_pairs):
        """
        Return query expansions given list of word-tag pairs 
        for a query that starts with a preposition.
        """
        first_tag, second_tag = [tag for word, tag in wt_pairs[:2]]
        expansions = []
        if first_tag == "IN" and second_tag in ["WP", "WDT"]:
            prep = wt_pairs[0][0].lower()
            words = self.common_transformations(wt_pairs, 2)
            words.append(prep)
            expansions.append(" ".join(words))
        return expansions

    def expand_what(self, wt_pairs):
        """
        Return query expansions given list of word-tag pairs 
        for a query that contains "what".
        """
        first_tag = wt_pairs[0][1]
        expansions = []
        if first_tag in ["WP", "WRB", "WDT"]:
            words = self.common_transformations(wt_pairs, 1)
            expansions.append(" ".join(words))
        return expansions

    def expand_where(self, wt_pairs):
        """
        Return query expansions given a list of word-tag pairs 
        for a query that contains "where", "when", or "how".
        """
        first_word, first_tag = wt_pairs[0]
        second_word, second_tag = wt_pairs[1]
        first_word = first_word.lower()
        
        expansions = []
        if first_tag == "WRB":
            words = self.common_transformations(wt_pairs, 1)
            # "when", "where" question
            if first_word in self.wh_classes:
                # produce expansions for each synonym of "where" or "when"
                for wh_synonym in self.wh_classes[first_word]:
                    expansions.append(" ".join(words + [wh_synonym]))
            # "how" question
            elif first_word == "how":
                if second_word in ["many", "far"]:
                    words = words[1:]
                    if second_word == "many":
                        expansions.append(" ".join(["there", "are"] + words))
                    else:
                        for distance_word in self.distance_words:
                            expansions.append(" ".join(words + [distance_word]))
        return expansions

    def remove_save_first_instance(self, wt_pairs, targets):
        """
        Given a list of word-tag pairs, remove first occurrence 
        of word-tag pair whose tag is in target tags. Return removed 
        word and modified copy of w-t pairs.
        """
        for i, pair in enumerate(wt_pairs):
            word, tag = pair
            if tag in targets:
                return (word, wt_pairs[:i] + wt_pairs[i+1:])
        return ("", wt_pairs)

    def fix_possessives(self, s):
        """
        Return a version of the given string with 
        the following substitution: s/ 's/'s/g.
        """
        return re.sub(r" 's", r"'s", s)

    def common_transformations(self, wt_pairs, n):
        """
        Perform transformations of word-tag pair list 
        common to all query expansion types.

        Remove first n word-tag pair of wt_pairs, 
        return list of words with first verb moved to end.
        """
        wt_pairs = wt_pairs[n:]
        verb, wt_pairs = self.remove_save_first_instance(wt_pairs, self.verb_tags)
        words, tags = map(list, zip(*wt_pairs))
        if verb:
            words.append(verb)
        return words

    def strip_final_punc(self, wt_pairs):
        """
        Remove last word-tag pair of list of 
        such pairs if tag in the pair is sentence-final 
        punctuation.
        """
        if len(wt_pairs) < 1:
            return []
        else:
            last_tag = wt_pairs[-1][1]
            if last_tag in self.final_punc_tags:
                return wt_pairs[:-1]
            else:
                return wt_pairs