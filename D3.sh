#!/usr/bin/env bash

# LING 573
# Deliverable #3
# Nicholas Waltner
# Stefan Behr
# Tristan Bodding-Long
# master shell script

# run system and output results
time python2.7 src/QA_system.py -a /dropbox/12-13/573/Data/patterns/devtest/factoid-docs.litkowski.2006.txt -q /dropbox/12-13/573/Data/Questions/devtest/TREC-2006.xml -r Deliv3 -o outputs/D3.outputs

# evaluate results strict and lenient
echo "lenient"
tail -n2 results/D3.results_lenient
echo "strict"
tail -n1 results/D3.results_strict
#python2.6 /opt/dropbox/12-13/573/code/compute_mrr.py /dropbox/12-13/573/Data/patterns/devtest/factoid-docs.litkowski.2006.txt outputs/D2.outputs lenient > results/D2.results_lenient
#python2.6 /opt/dropbox/12-13/573/code/compute_mrr.py /dropbox/12-13/573/Data/patterns/devtest/factoid-docs.litkowski.2006.txt outputs/D2.outputs strict > results/D2.results_strict