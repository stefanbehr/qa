#!/usr/bin/env bash

# LING 573
# Deliverable 4
# Nicholas Waltner
# Stefan Behr
# Tristan Bodding-Long
# master shell script for the devset

mv src/ACQUAINT2006 src/ACQUAINT
#rm -r src/ACQUANT2006

# run system and output results
time python2.7 src/QA_system.py -c /corpora/LDC/LDC02T31 -a /dropbox/12-13/573/Data/patterns/devtest/factoid-docs.litkowski.2006.txt -q /dropbox/12-13/573/Data/Questions/devtest/TREC-2006.xml -r D4Dev -o outputs/2006

# evaluate results strict and lenient
echo "lenient"
tail -n2 results/2006_lenient
grep -o "	1.0" results/2006_lenient | wc -w
echo "strict"
tail -n1 results/2006_strict
grep -o "	1.0" results/2006_strict | wc -w

#python2.6 /opt/dropbox/12-13/573/code/compute_mrr.py /dropbox/12-13/573/Data/patterns/devtest/factoid-docs.litkowski.2006.txt outputs/D3.outputs lenient > results/D3.results_lenient
#python2.6 /opt/dropbox/12-13/573/code/compute_mrr.py /dropbox/12-13/573/Data/patterns/devtest/factoid-docs.litkowski.2006.txt outputs/D3.outputs strict > results/D3.results_strict

mv src/ACQUAINT src/ACQUAINT2006
#rm -r src/ACQUAINT